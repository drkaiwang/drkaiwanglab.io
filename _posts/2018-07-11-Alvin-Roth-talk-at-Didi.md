---
layout: post
title: 诺贝尔经济学奖得主Alvin Roth访问滴滴
categories: News
tags: 滴滴 诺贝尔奖 讲座 经济学
---

* content
{:toc}

诺贝尔经济学奖得主罗斯教授做客滴滴大咖分享，发表“市场和交易机制转型“主题报告，分享了他关于科技带来的市场和交易场所变革的观点，以及对于未来交通的设想。
罗斯教授是市场设计理论的开创者之一，滴滴出行平台是典型的双边市场（two-sided market），这些市场的设计和优化都与市场设计密切相关。
![诺贝尔经济学奖得主滴滴讲座](https://wx4.sinaimg.cn/mw690/007b40Iily1ftd92q3yl8j31hc140ah5.jpg)
![诺贝尔经济学奖得主滴滴讲座](https://wx4.sinaimg.cn/mw690/007b40Iily1ftd9lo6vbkj31hc14079e.jpg)



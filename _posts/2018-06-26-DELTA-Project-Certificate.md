---
layout: post
title: DELTA计划终于毕业了！
categories: News
tags: 滴滴 数据科学 DELTA计划 毕业 证书 
---

* content
{:toc}

![KAI-WANG-DELTA-Project-Certificate](https://gitlab.com/drkaiwang/images_2019/wikis/uploads/5a5c41bc879add7400217659c479f663/007b40Iily1ftd92q56abj31hc140n1e.jpg)
- DELTA计划是滴滴内部的数据科学培训计划，经过半年以来的多次培训，完成了36学分的学习，成为首批毕业的小桔子！
- 以后要在工作中更加努力地学习进步！

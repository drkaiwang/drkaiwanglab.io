---
layout: post
title: 【汇总】币圈最近的糖果汇总，拿走不谢！
categories: News
tags: 区块链 币圈 免费 糖果 汇总 Blockchain
---

* content
{:toc}

写在前面：第一次接触币圈的朋友们，不建议你们第一次上来就买币，免费领一些，对币圈了解透彻了，有了对项目的辨别能力在去买币，现在不如发挥你们所拥有的优势（时间多、朋友多）撸一撸区块链的羊毛，耐心撸下来，每月的生活费是不愁的。

----------

### 【糖果1：SUNBOX.one糖果平台】

SUNBOX.one是一个全球加密货币的糖果平台，平台代币SUN Token的发行量为800亿枚，SUNBOX团队在未来将会给SUN注入更多价值，让支持者和分享者能持续获益。

SUNBOX将计划基于EOS来进行项目开发，并且未来会兼容更多基于EOS开发的项目，让大家可以获得更多EOS项目糖果（除了项目方空投外，我们额外再提供的糖果）。但SUNBOX未来派发的糖果并不局限在EOS生态内，还会派发不同公链下的项目糖果，所以持有SUN币，将会获得更多项目糖果的空投。

初期我们会启动SUN的赠送活动，而且用户之间可以在SUNBOX里进行转赠。后续我们会把SUN区块链化，让SUN成为SUNBOX区块链合作体系的流通资产。

SUN的价值：

1、兑换主流币：通过平台合作等方式获得的主流币，将会放在SUNBOX里给大家用SUN来兑换，兑换会抵消相应数量的SUN。

2、免费领取空投糖果：只要账户余额的SUN达到指定数量，系统在空投时会对SUN的余额进行快照，达到要求的即可参与平分每次合作方的空投糖果，空投糖果不会抵消SUN。

3、糖果盒升值：SUNBOX的糖果盒会有越来越多的主流币锁定在这里，让每一个SUN都有价值。而且随着糖果盒里的糖果升值，SUN也会跟着升值。

4、优先参与活动：可以参与优质项目代币的首发活动，并且兑换顶级区块链交流会议的门票等。

5、跨平台流通：SUN会在多个项目平台上作为运行资源实现流通，SUN将会成为最具升值潜力的区块链资产。

更多价值正在开发中，SUN持有越多，价值越大。

**领取300个SUN币：[https://sunbox.one/i/kxaPlfRLm][1] （点击免费领取）**


----------


### 【糖果2：Prabox糖果百万大礼包，可在线挖糖果】

ProChain认为好的区块链项目要有好的市场营销和广告推广，所以PRA般若是基于以太坊、雷电网络、IPFS技术开发的区块链精准广告系统，利用区块链技术数据透明、不可篡改的特性，在数字广告行业重新建立共识机制，帮助多方降低成本，提高营销推广的精准效果。为了实现这些想法ProChain推出了糖果营销应用Prabox，Prabox提供糖果挖矿功能，在你挖代币糖果的同时帮助项目方进行推广。

Prabox是PRA般若体系下的首个移动端产品，集合了般若ID、钱包、糖果盒等功能于一体，以轻量便捷的方式为用户提供基于区块链应用的服务。现在注册即可领取Prabox的百万糖果大礼包，并且每天还可以挖取多种代币糖果，而且实名认证后，挖矿速度会增加，当然不认证也可以挖矿，只是挖矿速度会较慢。

**领取百万糖果：[https://prabox.net/][2] （点击免费领取）**

Prabox玩法大升级：

1. 糖果领取机制升级为了随机+暴击的机制，每次点击领取到的糖果数量会在一定范围内随机。

2. 持有PRA的用户将会获得暴击效果，持有PRA越多，暴击几率越大。

**领取百万糖果：[https://prabox.net/][3] （点击免费领取）**


----------


### 【糖果3：超级柚糖果】

超级柚是基于EOS的共建区块链社区项目，有点类似现在的Gram plus基于Telegram的加密社区一样，其发行的Super EOS是EOS的周边生态代币。Super EOS总量为1000亿枚，100%将免费赠送给推广者和注册者，真正共享共建。推广截止日期为糖果送完的日期，超级柚的目标是打造完善的EOS的生态环境，超级柚将积极联系主流平台支持。并且自身也会建设多个渠道供其流通，并支持其应用。

Super EOS的token将基于EOS开发，并且拥有收益分红属性。Super EOS分发时间为EOS主链上线后两周。在此之前，你的token凭证将永久被保留在网站中，代币上线后，立即可提现至你的钱包。

现在注册，即送10000枚Super EOS，并且支持六度人脉邀请制，享受六级邀请赠送。

**领取Super EOS糖果：[http://speos123.com/][4]  （送10000枚）**


----------


### 【糖果4：EOZ糖果】

EOZ糖果总共500亿枚，全部免费赠送给推广者和注册者，推广日期为500亿枚全部送出截止。我们将积极联系主流平台支持，并且自身也会开设全方位多渠道供其流通和生产环境下的应用。

EOZ糖果数据将永久保留，eos主链上线三周内，我们将发行EOZ TOKEN，届时有详细教程教大家如何提币至钱包。

活动：注册即送5000枚，邀请m1送5000枚，邀请m2送2500枚，邀请m3送1250枚，邀请m4送600枚，邀请m5送300枚，邀请m6送150枚。

**领取EOZ糖果：[http://eoz.one/][5] （送5000枚）**


----------


### 【糖果5：李笑来的糖果Candy Box】

数字货币在今年迎来了爆发式增长，全世界都为之不眠。很多项目得到大家的支持，他们和我们的想法一样，希望在2018年的中国春节，回馈大家最喜爱的糖果。我们结盟了很多项目方，准备一起为大家带来一场史上最大的糖果盛宴，包括ATN (AT Network)、MDT (Measurable Data Token)、PIX (Lampix)、BIG (BIG)、FAIR (FAIR)、GCS (GCS)、HMC (HMC)、PRS (PRS)、TCT (Token Club)等，有不少都是李笑来投资的，总值34,662,731.20美元约2亿人民币。

**领取Candy Box糖果：[https://candy.one/][6]  （看到能领就抓紧领取了）**


----------


### 【糖果6：李笑来做顾问的Insurchain上线，每天挖矿收币】

InsurChain有点类似Medishares都是提供去中心化区块链保险服务的项目，但是Medishares偏向C2C实现保险运作，而InsurChain偏向与传统保险公司合作，把保险公司原有的保险产品通过InsurChain在区块链上提供保险服务。所以Medishares更像是保险业的淘宝，而InsurChain更像是保险业的天猫和京东。

InsurChain获得了硬币资本、连接资本、星链资本、曲速资本、合约资本、天奇创投的投资，并且还获得李笑来和林嘉鹏作为顾问，可以说项目可行性还是比较高的。现在InsurChain正式发布其产品InsurWallet，通过手机挖矿即可获得其代币insur，每天挖矿每天收币。目前insur已经上线okex交易平台，现在挖矿人数不多，先挖多得。

**注册挖矿地址：[http://mrw.so/53A2gA][7]  （点击领取）**


----------


### 【糖果7：GiftOne糖果】

Gift.One是世界上最大的加密货币空投机构。 Gift.One与各种项目合作，包括Telegram将其空投tokens锁定到Gift Box内并分发Gift.One Token（Gift）。 Gift将通过空投，礼品，彩票和秒杀等多种方式陆续分发给支持项目的用户。 目前Gift Box锁定的将近20个Token包括TON，ETH和EOS，总计近亿美元。

**领取Gift糖果：[https://gift.one/i/][8]  （点击领取）**



  [1]: https://sunbox.one/i/kxaPlfRLm
  [2]: https://prabox.net/tpl/reg.html?i=RSQN
  [3]: https://prabox.net/tpl/reg.html?i=RSQN
  [4]: http://speos123.com/i/709282/
  [5]: http://eoz.one/i/2174266
  [6]: https://candy.one/i/2350684
  [7]: http://mrw.so/53A2gA
  [8]: https://gift.one/i/szt9KI
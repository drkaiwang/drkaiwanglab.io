---
layout: post
title:  区块链段子一则
categories: Notes
tags: 段子 区块链 泡沫 邓紫棋 澳门 Blockchain
---

* content
{:toc}

今天的2018第一届世界区块链大会（澳门站）上，主办方邀请邓紫棋上台演唱《泡沫》：“阳光下的泡沫，是彩色的。就像被骗的我，是幸福的”

![第一届世界区块链大会-泡沫](http://wx3.sinaimg.cn/large/b89e3f89gy1fqnuvu08m2j20u00k0ndz.jpg)
---
layout: post
title: 如何利用MyEtherWallet生成以太坊代币钱包
categories: Tech
tags: 区块链 币圈 MyEtherWallet ETH 以太坊 钱包 Blockchain
---

* content
{:toc}

![MyEtherWallet以太坊代币钱包][1]
------
### MyEtherWallet特点：
 - 简单易用：MyEtherWallet网页钱包是使用起来最简单的钱包，对新手比较友好，只需要打开网页就可以使用。
 - 代码开源：MyEtherWallet代码开源，供大家审视，用户下载源代码在自己电脑上使用，可以离线生成钱包和离线发送。
 - 去中心化：MyEtherWallet是去中心化的钱包，它不会存储用户的钱包信息账号，就算有一天MyEtherWallet网站不能使用，你也可以通过钱包的私钥和密码在其他钱包上找回你的钱包账号，安全性高。
 - 进阶可以使用metamask这样的以太坊钱包插件。

>- 官网地址：https://www.myetherwallet.com/
>- 官方代码下载地址：https://github.com/kvhnuke/etherwallet/releases/tag/v3.21.03
>- 中文版地址：http://www.yunyitai.com/


### 生成钱包步骤：

 1. Go to https://www.myetherwallet.com/ . 
 2. Enter a strong but easy to remember password. 
 3. Click the "Create New Wallet" button.
 4. Click the "Download" button & save your Keystore / UTC file. Back it up. 
 5. Read the warning. If you understand it and promise not to lose your private key, click the "I understand. Continue" button. 
 6. You now have the option of printing a paper wallet, saving your private key, or saving a QR code of your private key. Back up at least one (offline). 
 7. Then click "Next: Save your Address"
 8. Unlock your walletthat you just created using the Keystore / UTC file you just downloaded or the private key.
 9. Save your address to a text document & bookmark the link to it on https://etherscan.io/.
 10. Ensure all information matches. Don't lose this information. Double check your work. Don't be dumb.

### 最终生成的ETH钱包地址：
> 0x65D52f1639B720AB1f957AF2E683990BB21203ee

  [1]: https://wx2.sinaimg.cn/mw1024/741d838bly1frdl43fdigj20pu0famyc.jpg
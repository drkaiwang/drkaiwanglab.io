---
layout: post
title: 滴滴六周年生日快乐！
categories: Tech
tags: 滴滴 六周年 生日快乐 海报
---

* content
{:toc}


![滴滴六周年][1]{:width="500px"}
![滴滴六周年][2]{:width="500px"}
![滴滴六周年][3]{:width="500px"}
![滴滴六周年][4]{:width="500px"}
![滴滴六周年][5]{:width="500px"}
![滴滴六周年][6]{:width="500px"}
![滴滴六周年][7]{:width="500px"}
![滴滴六周年][8]{:width="500px"}
![滴滴六周年][9]{:width="500px"}


  [1]: https://wx1.sinaimg.cn/large/741d838bly1fs1wnqxh04j20eq0m8wij.jpg
  [2]: https://wx4.sinaimg.cn/large/741d838bly1fs1wnrrxbcj20eq0m8n17.jpg
  [3]: https://wx4.sinaimg.cn/large/741d838bly1fs1wnrn1ctj20eq0m8n0j.jpg
  [4]: https://wx4.sinaimg.cn/large/741d838bly1fs1wns1raoj20eq0m8jvy.jpg
  [5]: https://wx1.sinaimg.cn/large/741d838bly1fs1wnrwbdmj20ku0veq6r.jpg
  [6]: https://wx3.sinaimg.cn/large/741d838bly1fs1wnsag1uj20ku0ve7ac.jpg
  [7]: https://wx3.sinaimg.cn/large/741d838bly1fs1wnrso7nj20eq0m8tbw.jpg
  [8]: https://wx1.sinaimg.cn/large/741d838bly1fs1wnsfmtbj20ku0vetdj.jpg
  [9]: https://wx3.sinaimg.cn/large/741d838bly1fs1wns9fctj20ku0vedl0.jpg

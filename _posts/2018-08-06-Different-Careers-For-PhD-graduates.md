---
layout: post
title: 《Nature》- 离开学术圈的博士生并不失败
categories: Notes
tags: Hive Bug Tableau Null PARTITION 字段 分区
---

* content
{:toc}


![it is not a failure to leave academia][1]

最近在Nature上看到了一篇评论：[Why it is not a 'failure' to leave academia][2]，谈到的恰好是博士毕业后转行的话题，对想要转行的博士生给了几点非常中肯的建议。

笔者是工科博士，毕业回国后几经辗转到了互联网行业做数据科学方向的工作，中间也历经了不少辛酸和挫折。的确很多人会对我说，读了名校的博士，不做科研/不进高校可惜了啊。其实我想对他们说：只要能够及时发现自己的真正兴趣，并且现在和将来能够从事自己真正热爱的工作和事业，那么不管成本多大、过程多么艰辛都是值得的。况且，读博三年多的学术训练和留学经历也是我一生中最宝贵的财富，一点都不浪费。

引用原文中的话来说：

> A PhD is highly valuable
>
> This leaves us with one last aspect of the culture of failure and its effect on doctoral students and postdocs: the widespread misconception that a PhD is useful training only for academic research. Or, in other words, if you leave academia, your mum will think that you’ve wasted your time doing a PhD. You might even have wondered about that yourself.
>
> We know that most PhD graduates eventually go on to other careers, but have they all wasted their time? Absolutely not. The skills you are acquiring (or have acquired) during a PhD are highly sought by employers beyond academic science. **You are incredibly resilient, hard-working and motivated. You make decisions based on evidence, you can interpret data, you can communicate complex concepts clearly, you are an effective team player and you can prioritize tasks. And you have a degree to prove all of this.**
>
> You have every reason to be positive about your job prospects.
>
>**Personally, I won't regret having done my PhD, regardless of my future career.**

  [1]: https://gitlab.com/drkaiwang/images_2019/wikis/uploads/a08ccf7f2d88b5171dedc217c7ddb9d3/007b40Iily1fu0e8sn5ddj30k80f6my3.jpg
  [2]: https://www.nature.com/articles/d41586-018-05838-y
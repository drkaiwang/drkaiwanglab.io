---
layout: post
title: HIVE表新增加字段导致新增字段为NULL值的BUG
categories: Notes
tags: Hive Bug Tableau Null PARTITION 字段 分区
---

* content
{:toc}


> 问题描述：
> 数据分析时经常遇到需求变更等特殊情况，需要在hive表中增加字段，并且重跑调度任务回溯历史数据（insert overwrite），但连接tableau时发现新增字段均为null值
 
#### 解决方法1：
第一步: 在hive元数据中的sds表找到字段增加后新分配的字段组ID(CD_ID，表的所有字段对应一个CD_ID字段值)，如:SELECT * FROM sds WHERE location LIKE '%table_name%'
第二步: 在SDS表中可以看到新分配的字段组值(CD_ID)、已有分区所对应的旧字段组值ID(CD_ID)，在该表中把旧的CD_ID值更新为新的CD_ID值即可，如:UPDATE SDS SET CD_ID=NEW_CD_ID(所找到的新值) WHERE CD_ID=OLD_CD_ID(旧值)
 
#### 解决方法2：
删除原有分区后再回溯历史数据；或删除原表重新建表后回溯历史数据。


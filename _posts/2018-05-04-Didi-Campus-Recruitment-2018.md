---
layout: post
title: 滴滴2018应届生校招管培生职位内推
categories: Jobs
tags: 滴滴 校园招聘 内推 职位 管培生 应届生 简历
---

* content
{:toc}

![滴滴2018校招](http://campus.didichuxing.com/static/media/banner.0028c626.jpg)

内推职位申请方法：直接点击下列内推专属链接查看和投递简历

#### [（应届内推专用链接）客户服务管培生-综合业务大数据方向--CTO](http://www.hotjob.cn/wt/mailResponse/juxian/positions/showOpenPostDetail?corpCode=1037ec9c1de5d24c366156da80dc4a9c&paramStr=a9dbed4834384dab8bb91c08e6bcf58f)
#### [（应届内推专用链接）运营管培生--快捷出行](http://www.hotjob.cn/wt/mailResponse/juxian/positions/showOpenPostDetail?corpCode=1037ec9c1de5d24c366156da80dc4a9c&paramStr=a9dbed4834384dab3f88b6a5af14ae38)
#### [（应届内推专用链接）数据分析管培生--快捷出行](http://www.hotjob.cn/wt/mailResponse/juxian/positions/showOpenPostDetail?corpCode=1037ec9c1de5d24c366156da80dc4a9c&paramStr=a9dbed4834384dab296686bd900790d9)
#### [（应届内推专用链接）市场管培生--快捷出行](http://www.hotjob.cn/wt/mailResponse/juxian/positions/showOpenPostDetail?corpCode=1037ec9c1de5d24c366156da80dc4a9c&paramStr=a9dbed4834384daba6c2d9a8bdd8bc47)
#### [（应届内推专用链接）客户服务管培生-海外业务--CTO](http://www.hotjob.cn/wt/mailResponse/juxian/positions/showOpenPostDetail?corpCode=1037ec9c1de5d24c366156da80dc4a9c&paramStr=a9dbed4834384dab5c4699de0ff35683)
#### [（应届内推专用链接）客户服务管培生--CTO](http://www.hotjob.cn/wt/mailResponse/juxian/positions/showOpenPostDetail?corpCode=1037ec9c1de5d24c366156da80dc4a9c&paramStr=a9dbed4834384dab4f6483773c7c710e)

开放日期: 2018.4.9--2018.5.31 
面向群体: 2018届国内外毕业生（2017.9--2018.9）

#### 网申规则
每位同学可选择“普通网申”和“内部推荐”两种申请方式:
1. 内部推荐：每位同学可以被滴滴内部同事推荐一个职位，先被内推的同学仍可通过“普通网申”申请其他职位，优先处理内推志愿，内推志愿不合适将自动流转至网申志愿
2. 普通网申：每位同学可申请两个职位，优先处理第一志愿，第一志愿不合适后将自动流转至第二志愿

#### 注意事项
1. 参与“内部推荐”的同学仍可以参与“普通网申”，“内部推荐”志愿将被优先处理 
2. 已参与“普通网申”的同学不能再被“内部推荐” 
3. 任一志愿职位进入甄选流程时，其它志愿将被锁定 
4. 请同学们慎重选择合适自己的职位，完善个人信息，保证信息真实有效 

